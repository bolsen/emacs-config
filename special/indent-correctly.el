(defun indent-correctly ()
  "Indents the way I want it to. For use with TAB, it doesn't blindly
stick in a tab character, but instead cleans up the indentation f"
  (interactive "*")
  (delete-horizontal-space)
  (indent-according-to-mode)
  )

(defun insert-a-tab ()
  (interactive "*")
  (insert-char "\t" 1)
  )

(global-set-key (kbd "C-c TAB") 'indent-correctly)
(global-set-key (kbd "C-x TAB") 'insert-a-tab)
