;; (add-to-list 'load-path "~/.emacs.d/vendor/textmate.el")
;;(require 'textmate)
;;(textmate-mode)

;; Enter into fullscreen mode; will probably break aquamacs so be careful

(define-key global-map (kbd "S-s-<return>") 'ns-toggle-fullscreen)

;; Shrink and grow window

(define-key global-map (kbd "C-S-<down>") 'shrink-window)
(define-key global-map (kbd "C-S-<up>") 'enlarge-window)
(define-key global-map (kbd "C-S-<left>") 'shrink-window-horizontally)
(define-key global-map (kbd "C-S-<right>") 'enlarge-window-horizontally)

(defun ri-bind-key ()
  (local-set-key "\C-c\C-l" 'yari)
  )
(add-hook 'ruby-mode-hook 'ri-bind-key)

;; autoindent
(define-key global-map (kbd "RET") 'newline-and-indent)

;; rvm + inf-ruby

(add-to-list 'load-path "~/EmacsConf/custom/special/rvm.el")
(require 'rvm)
(rvm-use-default)

(load-file "~/EmacsConf/custom/special/autotest.el")
(require 'autotest)

(setq auto-mode-alist (cons '("\\.mdown" . markdown-mode) auto-mode-alist))

(add-to-list 'load-path "~/EmacsConf/custom/special/jd-el")
(require 'google-maps)

(setq tab-width 4)

;;(require 'ruby-electric)
;;(add-hook 'ruby-mode-hook (lambda () (ruby-electric-mode t)))

;; RHTML additions
(add-to-list 'auto-mode-alist '("\\.erb$" . rhtml-mode))


;; for ido support with running commands in M-x
(setq ido-execute-command-cache nil)

(defun ido-execute-command ()
  (interactive)
  (call-interactively
   (intern
    (ido-completing-read
     "M-x "
     (progn
       (unless ido-execute-command-cache
         (mapatoms (lambda (s)
                     (when (commandp s)
                       (setq ido-execute-command-cache
                             (cons (format "%S" s) ido-execute-command-cache))))))
       ido-execute-command-cache)))))

(add-hook 'ido-setup-hook
          (lambda ()
            (setq ido-enable-flex-matching t)
            (global-set-key "\M-x" 'ido-execute-command)))


;; nXhtml blah

;; (load "~/EmacsConf/custom/nxhtml/autostart.el")

;; (setq
;;  nxhtml-global-minor-mode t
;;  mumamo-chunk-coloring 'submode-colored
;;  nxhtml-skip-welcome t
;;  indent-region-mode t
;;  rng-xnml-auto-validate-flag nil
;;  nxml-degraded t)

;; (add-to-list 'auto-mode-alist '("\\.html\\.erb\\'" . eruby-nxhtml-mumamo-mode))

;; We replace with rhtml

(add-to-list 'load-path "~/EmacsConf/custom/special/rhtml")
(require 'rhtml-mode)


(menu-bar-mode t)


;; Turn on tabs
(setq indent-tabs-mode nil)
(setq-default indent-tabs-mode nil)


;; Bind the TAB key
(global-set-key (kbd "TAB") 'self-insert-command)

;; Set the tab width
(setq default-tab-width 4)
(setq tab-width 4)
(setq c-basic-indent 4)

;; Scala mode

(add-to-list 'load-path "~/EmacsConf/custom/special/scala-mode")
(require 'scala-mode-auto)
;; (setq yas/my-directory "~/EmacsConf/custom/scala-mode/contrib/yasnippet/snippets")
;; (yas/load-directory yas/my-directory)

(add-hook 'scala-mode-hook '(lambda () (yas/minor-mode-on)))


;; Full Ack

(autoload 'ack-same "full-ack" nil t)
(autoload 'ack "full-ack" nil t)
(autoload 'ack-find-same-file "full-ack" nil t)
(autoload 'ack-find-file "full-ack" nil t)


;; This is freakin' essential to end the whitespace madness with git

;; (add-hook 'before-save-hook 'delete-trailing-whitespace)

;; alerts when there is a tab (with a nasty red space)
(require 'show-wspace)


;; register
(global-set-key (kbd "C-x j r") 'register-to-point)

;; turn off auto-fill-mode!
(auto-fill-mode 'f)


;; Total replacement of starter-kit-js:

;;; starter-kit-js.el --- Some helpful Javascript helpers
;;
;; Part of the Emacs Starter Kit

(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.json$" . js2-mode))


;;; starter-kit-js.el ends here

(add-to-list 'load-path "~/EmacsConf/custom/rvm.el/")
(require 'rvm)

(defun bv-sys ()
  (interactive)
  (rvm-use "ree-1.8.7-2010.02" "*default*")
  (dired "~/beenverified/system")
  )

(defun bv-mail ()
  (interactive)
  (rvm-use "ruby-1.9.2-p0" "*default*")
  ( dired "~/beenverified/bvmailer")
  )

(defun go-to-bv (path)
  (interactive "s")
  (rvm-use "ree" "beenverified")
  (dired (concat "~/beenverified/" path))
  ;;  (Split-window-horizontally)
  ;; (magit-status (concat "~/beenverified/" path))
  )

(global-set-key (kbd "C-x C-2 C-b") 'bv-sys)
(global-set-key (kbd "C-x C-3 C-b") 'bv-mail)

(defun qwandry (name)
  (interactive "sName: ")
  (shell-command (concat "qw " "-e openemacs " name))
  )

;; useful for running autotest
(defun touch-file-for-current-buffer ()
  (interactive)
  (shell-command (concat "touch " (buffer-file-name)))
  (revert-buffer 't 't 't)
  )

(global-set-key (kbd "C-c t") 'touch-file-for-current-buffer)


(require 'multi-term)
(setq multi-term-program "/bin/bash")


(defun toggle-current-window-dedication ()
  (interactive)
  (let* ((window    (selected-window))
         (dedicated (window-dedicated-p window)))
    (set-window-dedicated-p window (not dedicated))
    (message "Window %sdedicated to %s"
             (if dedicated "no longer " "")
             (buffer-name))))


(defadvice multi-term (around multi-term-dedicate)
  (toggle-current-window-dedication)
  ad-do-it
  (toggle-current-window-dedication)
  )   

(ad-update 'multi-term)


(global-set-key (kbd "C-x : d") 'toggle-current-window-dedication)

(add-to-list 'load-path "~/EmacsConf/custom/special/indent-correctly.el")



;; FOR ORG
(org-indent-mode)


;; (add-to-list 'load-path "~/EmacsConf/custom/android-mode")
;; (require 'android-mode)
;; (defcustom android-mode-sdk-dir "/usr/local/Cellar/android-sdk/r7/")


;; java annotations
(require 'java-mode-indent-annotations)

(setq java-mode-hook
      (function (lambda ()
                  (java-mode-indent-annotations-setup))))

;;(add-to-list 'load-path "~/EmacsConf/custom/rinari")
;;(require 'rinari)


;; escreen, back in black
(load "escreen")
(escreen-install)

;; ;; eclim

;; (add-to-list 'load-path (expand-file-name "~/EmacsConf/custom/emacs-eclim/"))
;;  ;; only add the vendor path when you want to use the libraries provided with emacs-eclim
;; (add-to-list 'load-path (expand-file-name "~/EmacsConf/custom/emacs-eclim/vendor"))
;; (require 'eclim)

;; (setq eclim-auto-save t)
;; (global-eclim-mode)
;; (require 'company-emacs-eclim)
;; (company-mode)
;; g(company-emacs-eclim-setup)
;; (define-key global-map (kbd "C-c C-/") 'company-complete)


;; for haskell
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
(add-hook 'haskell-mode-hook 'haskell-font-lock-symbols t)
(put 'downcase-region 'disabled nil)



;; for rails yasnippet
(load-file "~/EmacsConf/custom/special/yasnippets-rails/setup.el")


;; will this solve the auto-fill-mode issue?

(set-default 'word_wrap nil)


(remove-hook 'text-mode-hook 'auto-detect-wrap)
(remove-hook 'text-mode-hook 'turn-on-auto-fill)


;; erlang

(setq load-path (cons  "/usr/local/Cellar/erlang/R14B01/lib/erlang/lib/tools-2.6.6.2/emacs"
                       load-path))
(setq erlang-root-dir "/usr/local/Cellar/erlang/R14B01")
(setq exec-path (cons "/usr/local/Cellar/erlang/R14B01/bin" exec-path))
(require 'erlang-start)


;; package archive change
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/") t)


;; Delete selection mode

(delete-selection-mode 1)


;; I dont know why yaml mode uses tabs, but ...

(setq yaml-mode-hook
      (function (lambda ()
                  (setq indent-tabs-mode nil)
                  (setq c-indent-level 4))))


(setq term-default-bg-color nil)
(setq term-default-fg-color "#AAAAAA")

;; campfire

;; (require 'campfire)

;; (setq campfire-room-id "368558")

;; (defvar campfire-ssl "true")
;; (defvar campfire-token "


;; winner mode

(winner-mode 1)


;; It seems like javascript-mode was not loading up on .js files.
;; (add-to-list 'auto-mode-alist '("\\.js$" . javascript-mode))


;; enclose mode - the absolutely perfect perfect punctuation closer
;; can apply this to any mode that it is needed in, and the behavior
;; is just better than ruby-electric that made me go insane.
;; (add-hook 'ruby-mode-hook 'enclose-mode)
;; (add-hook 'rhtml-mode-hook 'enclose-mode)
(require 'autopair)
(autopair-global-mode)
(setq autopair-autowrap t)

;; rgrep hook
(define-key global-map (kbd "C-c ' g") 'rgrep)

;; cua?
;; (cua-mode)



;; (defun ruby-indent-line (&optional flag)
;;   "Correct the indentation of the current ruby line."
;;   (interactive)
;;   (ruby-indent-to (ruby-calculate-indent)))

;; (defun ruby-indent-to (column)
;;   "Indent the current line to COLUMN."
;;   (when column
;;     (let (shift top beg)
;;       (and (< column 0) (error "invalid nest"))
;;       (setq shift (current-column))
;;       (beginning-of-line)
;;       (setq beg (point))
;;       (back-to-indentation)
;;       (setq top (current-column))
;;       (skip-chars-backward " \t")
;;       (if (>= shift top) (setq shift (- shift top))
;;         (setq shift 0))
;;       (if (and (bolp)
;;                (= column top))
;;           (move-to-column (+ column shift))
;;         (move-to-column top)
;;         (delete-region beg (point))
;;         (beginning-of-line)
;;         (indent-to column)
;;         (move-to-column (+ column shift))))))

;; (define-key global-map (kbd "TAB") 'ruby-indent-line)

(define-key global-map (kbd "C-c C-t") 'compile)

(require 'aes)

(textmate-mode)
(fringe-mode)

;; goes to end of line, opens a line goes down to the line.

(defun open-line-and-go-to-line ()
  (interactive)
  (move-end-of-line nil)
  (open-line 0)
  (reindent-then-newline-and-indent) 
  )

(define-key global-map (kbd "C-c C-o") 'open-line-and-go-to-line)

(add-hook 'rhtml-mode-hook
          (lambda()
            (setq sgml-basic-offset 4)
            (setq indent-tabs-mode t)))


(defun wrap-html-tag (tagName)
  "Add a tag to beginning and ending of current word or text selection."
  (interactive "sEnter tag name: ")
  (let (p1 p2 inputText)
    (if (use-region-p)
        (progn 
          (setq p1 (region-beginning) )
          (setq p2 (region-end) )
          )
      (let ((bds (bounds-of-thing-at-point 'symbol)))
        (setq p1 (car bds) )
        (setq p2 (cdr bds) ) ) )

    (goto-char p2)
    (insert "</" tagName ">")
    (goto-char p1)
    (insert "<" tagName ">")
    ))



;; WOAH it works
;; (add-hook 'lua-mode-hook
;;           '(lambda ()
;;              (add-hook 'before-save-hook
;;                        (lambda ()
;;                          (message "ok")
;;                          (untabify (point-min) (point-max))))))



;; (add-hook 'lua-mode-hook
;;           '(lambda () (setq indent-line-function 'ruby-indent-line)))


;; JSHint support
;; (setenv "PATH" (concat (getenv "PATH") "/usr/local/bin:"))
;; (setq exec-path
;;       '(
;;     "/usr/local/bin"
;;     "/usr/bin"
;;     ))


(add-hook 'js2-mode-hook
          '(lambda ()
			 (add-hook 'before-save-hook (lambda () (untabify-buffer)))))

(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))



;; Based on: http://mihai.bazon.net/projects/editing-javascript-with-emacs-js2-mode

(defun my-js2-indent-function ()
  (interactive)
  (save-restriction
    (widen)
    (let* ((inhibit-point-motion-hooks t)
           (parse-status (save-excursion (syntax-ppss (point-at-bol))))
           (offset (- (current-column) (current-indentation)))
           (indentation (espresso--proper-indentation parse-status))
           node)

      (save-excursion

        ;; I like to indent case and labels to half of the tab width
        (back-to-indentation)
        (if (looking-at "case\\s-")
            (setq indentation (+ indentation (/ espresso-indent-level 2))))

        ;; consecutive declarations in a var statement are nice if
        ;; properly aligned, i.e:
        ;;
        ;; var foo = "bar",
        ;;     bar = "foo";
        (setq node (js2-node-at-point))
        (when (and node
                   (= js2-NAME (js2-node-type node))
                   (= js2-VAR (js2-node-type (js2-node-parent node))))
          (setq indentation (+ 4 indentation))))

      (indent-line-to indentation)
      (when (> offset 0) (forward-char offset)))))

(defun my-indent-sexp ()
  (interactive)
  (save-restriction
    (save-excursion
      (widen)
      (let* ((inhibit-point-motion-hooks t)
             (parse-status (syntax-ppss (point)))
             (beg (nth 1 parse-status))
             (end-marker (make-marker))
             (end (progn (goto-char beg) (forward-list) (point)))
             (ovl (make-overlay beg end)))
        (set-marker end-marker end)
        (overlay-put ovl 'face 'highlight)
        (goto-char beg)
        (while (< (point) (marker-position end-marker))
          ;; don't reindent blank lines so we don't set the "buffer
          ;; modified" property for nothing
          (beginning-of-line)
          (unless (looking-at "\\s-*$")
            (indent-according-to-mode))
          (forward-line))
        (run-with-timer 0.5 nil '(lambda(ovl)
                                   (delete-overlay ovl)) ovl)))))

(defun my-js2-mode-hook ()
  (require 'espresso)
  (setq espresso-indent-level 2
        indent-tabs-mode nil
        c-basic-offset 2)
  (c-toggle-auto-state 0)
  (c-toggle-hungry-state 1)
  (set (make-local-variable 'indent-line-function) 'my-js2-indent-function)
  (define-key js2-mode-map [(meta control |)] 'cperl-lineup)
  (define-key js2-mode-map [(meta control \;)] 
    '(lambda()
       (interactive)
       (insert "/* -----[ ")
       (save-excursion
         (insert " ]----- */"))
       ))
  (define-key js2-mode-map [(return)] 'newline-and-indent)
  (define-key js2-mode-map [(backspace)] 'c-electric-backspace)
  (define-key js2-mode-map [(control d)] 'c-electric-delete-forward)
  (define-key js2-mode-map [(control meta q)] 'my-indent-sexp)
  (if (featurep 'js2-highlight-vars)
    (js2-highlight-vars-mode))
  (message "My JS2 hook"))

(add-hook 'js2-mode-hook 'my-js2-mode-hook)
