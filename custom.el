(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(android-mode-sdk-dir "/usr/local/Cellar/android-sdk/r8")
 '(aquamacs-additional-fontsets nil t)
 '(aquamacs-customization-version-id 210 t)
 '(aquamacs-tool-bar-user-customization nil t)
 '(compilation-auto-jump-to-first-error nil)
 '(compilation-context-lines nil)
 '(compilation-scroll-output t)
 '(cua-enable-cua-keys t)
 '(cua-mode t nil (cua-base))
 '(cua-remap-control-v nil)
 '(cua-remap-control-z nil)
 '(default-frame-alist (quote ((vertical-scroll-bars) (fringe) (right-fringe) (left-fringe . 1) (internal-border-width . 0) (cursor-type . box) (background-color . "#0C1021") (background-mode . dark) (border-color . "black") (cursor-color . "#A7A7A7") (foreground-color . "#F8F8F8") (mouse-color . "sienna1") (tool-bar-lines . 0))))
 '(display-time-mode t)
 '(ido-use-filename-at-point nil)
 '(js2-auto-indent-p t)
 '(js2-basic-offset 2)
 '(js2-bounce-indent-p nil)
 '(js2-enter-indents-newline t)
 '(js2-indent-on-enter-key t)
 '(js2-mirror-mode nil)
 '(js2-mode-indent-ignore-first-tab t)
 '(lua-indent-level 2)
 '(magit-git-executable "/opt/local/bin/git")
 '(ns-tool-bar-display-mode nil t)
 '(ns-tool-bar-size-mode nil t)
 '(org-modules (quote (org-bbdb org-bibtex org-gnus org-info org-jsinfo org-irc org-mac-message org-mew org-mhe org-rmail org-vm org-wl org-w3m org-mac-iCal)))
 '(ruby-deep-arglist nil)
 '(ruby-deep-indent-paren nil)
 '(server-host "127.0.0.1")
 '(server-use-tcp t)
 '(show-paren-mode t)
 '(visual-line-mode nil t))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(html-mode-default ((t (:inherit sgml-mode-default :height 120 :family "Monaco"))) t)
 '(rhtml-mode-default ((t (:inherit html-mode-default :height 120 :family "Monaco"))) t))





